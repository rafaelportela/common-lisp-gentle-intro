;;; chap 06.5 more functions on lists

;;; 6.6
(defun last-element (xs)
  (first (last xs)))

(defun last-element (xs)
  (first (reverse xs)))

(defun last-element (xs)
  (let ((len (length xs)))
    (if (> len 0)
      (nth (- (length xs) 1) xs)
      nil)))

;;; 6.7
(defun next-to-last (xs)
  (let ((len (length xs)))
    (if (> len 1)
      (second (reverse xs))
      nil)))

(defun next-to-last (xs)
  (let ((len (length xs)))
    (if (> len 1)
      (nth (- len 2) xs)
      nil)))

;;; 6.8
(defun my-butlast (xs)
  (remove (last-element xs) xs))

;;; 6.9
; resp: car

;;; 6.10 palindrome
(defun palindrome-iter-p (xs i)
  (cond ((< i 2) t)
        ((not (eq (first xs) (nth (- i 1) xs))) nil)
        (t (palindrome-iter-p (cdr xs) (- i 2)))))

(defun palindrome-p (xs)
  (palindrome-iter-p xs (length xs)))

;;; intersection
;;; 6.15
(defun contains-article-p (text)
  (let ((articles '(the a an)))
    (intersection text articles)))

(defun contains-article-p (text)
  (or (member 'the text)
      (member 'a text)
      (member 'an text)))

