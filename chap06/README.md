## Chap 6- List Data Structures

Lists are one-way chains of pointers. It's easy to add an element to the
_front_ of a list because what we're reallt doing is creating a new cons cell
whose cdr points to the existing list.
```
(cons 'w '(x y z)) => (w x y z) or (w . (x y z))
```

When we cons (a b c) onto d, it's the car of the new cell that points to the
old list (a b c); the cdr points to the symbol d.
```
(cons '(a b c) 'd) => ((a b c) . d)
```
The dot is necessary because the cons cell chain ends in an atom other than
NIL.

There is no direct way to add an element to the end of a list simply by
creating a new cons cell, because the end of the original list already points
to NIL. More sophisticated techniques must be used.

### 6.6- Lists as sets

member
intersection
