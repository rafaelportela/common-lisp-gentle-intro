# COMMON LIST: A Gentle Introduction to Symbolic Computation

Following http://stevelosh.com/blog/2018/08/a-road-to-common-lisp/

```
$ sudo apt-get install sbcl rlwrap
$ rlwrap sbcl

This is SBCL 1.3.14.debian, an implementation of ANSI Common Lisp.
More information about SBCL is available at <http://www.sbcl.org/>.

SBCL is free software, provided as is, with absolutely no warranty.
It is mostly in the public domain; some portions are provided under
BSD-style licenses.  See the CREDITS and COPYING files in the
distribution for more information.
* (load "hello.lisp")

T
* (hello)
What is your name?
Rafael
Hello, Rafael.
NIL
```
