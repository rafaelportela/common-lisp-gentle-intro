;;; chap 05 variables and side effects

;;; 5.6 a
(defun throw-die () (+ 1 (random 5)))

;;; 5.6 b
(defun throw-dice ()
  (list (throw-die) (throw-die)))

;;; 5.6 c
(defun snake-eye-p (throw)
  (and (= 1 (first throw))
       (= 1 (second throw))))

(defun boxcars-p (throw)
  (and (= 6 (first throw))
       (= 6 (second throw))))

;;; 5.6 d
(defun instant-win-p (throw)
  (let ((sum (+ (first throw) (second throw))))
    (or (= sum 7)
        (= sum 11))))

(defun instant-loss-p (throw)
  (let ((sum (+ (first throw) (second throw))))
    (or (= sum 2)
        (= sum 3)
        (= sum 12))))

;;; 5.6 e
(defun say-throw (throw)
  (cond ((snake-eye-p throw) 'snake-eye)
        ((boxcars-p throw) 'boxcars)
        (t (+ (first throw) (second throw)))))

;;; 5.6 f
(defun craps ()
  (let* ((tr (throw-dice))
         (result (cond ((instant-win-p tr) '(you win))
                       ((instant-loss-p tr) '(you lose))
                       (T (list 'your 'point 'is (say-throw tr))))))
    (append (list 'throw (first tr) 'and (second tr) '--) result)))

;;; 5.6 g
(defun try-for-point (point)
  (let* ((throw (throw-dice))
         (sum (+ (first throw) (second throw)))
         (desc (list 'throw (first throw) 'and (second throw) '--))
         (result (cond ((= point sum) '(you win))
                       ((= sum 7) '(you lose))
                       (t (list 'your 'point 'is sum)))))
    (append desc result)))
