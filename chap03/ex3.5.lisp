(defun half (x)
  (/ x 2))

(defun cube (x)
  (* x x x))

(defun onemorep (x y)
  (equal x (+ y 1)))

