(defun addlength (x)
  (cons (length x) x))

(addlength (addlength '(a b c)))
=> (4 3 A B C)

